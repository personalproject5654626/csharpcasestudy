public class Customer
{
    public string? Name { get; set; }
    public int NumberPhone { get; set; }
    public Customer()
    {

    }
    public Customer(string name, int phone)
    {
        Name = name;
        NumberPhone = phone;
    }
}