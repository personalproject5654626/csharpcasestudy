public class CustomerManagement
{
    Customer[] customers;
    private int top = -1;
    private int bot = -1;
    private int _numberElementOfCustomer;

    public CustomerManagement(int numberElementOfCustomer)
    {
        _numberElementOfCustomer = numberElementOfCustomer;
        customers = new Customer[_numberElementOfCustomer];
    }

    public void AddEmployeeToEnqueue(Customer customer)
    {
        if (IsEmpty())
        {
            top = 0; bot = 0;
            customers[bot] = customer;
            return;
        }
        if (IsFull())
        {
            throw new Exception("Enqueue us full");
        }
        bot++;
        customers[bot] = customer;
    }

    public Customer RemoveCustomerFromEnqueue()
    {
        if (IsEmpty())
        {
            throw new Exception("Enqueue is empty");
        }
        var CustomerAtTop = customers[top];
        if (top == bot)
        {
            top = -1;
            bot = -1;
        }
        else
        {
            top++;
        }
        return CustomerAtTop;
    }

    public Customer ShowInfoCustomerAtTop()
    {
        if (IsEmpty())
        {
            throw new Exception("Enqueue is empty");
        }
        return customers[top];
    }
    public int CheckAmountCustomerInEnqueue() => bot - top + 1;
    private bool IsFull() => bot - top + 1 == _numberElementOfCustomer;

    private bool IsEmpty() => top == -1 && bot == -1;

}