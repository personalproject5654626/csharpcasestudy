﻿void GetMenu()
{
    Console.WriteLine("******** Customer Management ***********");
    Console.WriteLine("1 | Add new customer to enqueue      |");
    Console.WriteLine("2 | Remove customer at top           |");
    Console.WriteLine("3 | Show infor customer at top       |");
    Console.WriteLine("4 | Check amount customer in enqueue |");
    Console.WriteLine("5 | Exit                             |");
}

int GetChoice()
{
    try
    {
        Console.Write("Enter chocie: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (Exception ex)
    {
        throw new Exception(ex.Message);
    }

}
// Customer InputCustomer()
// {
//     var existCustomer = new Customer();
//     for (int i = 0; i < 5; i++)
//     {
//         existCustomer = new Customer("Diamond", 1);
//         existCustomer = new Customer("Diamond", 2);
//         existCustomer = new Customer("Diamond", 3);
//         existCustomer = new Customer("Diamond", 4);
//         existCustomer = new Customer("Diamond", 5);
//     }
//         return existCustomer;

// }

int choice;
CustomerManagement customerManagement = new CustomerManagement(5);
try
{
    do
    {
        GetMenu();
        choice = GetChoice();
        switch (choice)
        {
            case 1:
                customerManagement.AddEmployeeToEnqueue(new Customer("Diamond", 1));
                customerManagement.AddEmployeeToEnqueue(new Customer("Diamond", 2));
                customerManagement.AddEmployeeToEnqueue(new Customer("Diamond", 3));
                customerManagement.AddEmployeeToEnqueue(new Customer("Diamond", 4));
                customerManagement.AddEmployeeToEnqueue(new Customer("Diamond", 5));
                break;
            case 2:
                var removedCustomer = customerManagement.RemoveCustomerFromEnqueue();
                Console.WriteLine($"Name: {removedCustomer.Name} | NumberPhone: {removedCustomer.NumberPhone}");

                break;
            case 3:
                var infoCustomer = customerManagement.ShowInfoCustomerAtTop();
                Console.WriteLine($"Name: {infoCustomer.Name} | NumberPhone: {infoCustomer.NumberPhone}");
                break;
            case 4:
                var checkedCustomer = customerManagement.CheckAmountCustomerInEnqueue();
                Console.WriteLine(checkedCustomer);
                break;
            case 5:
                Console.WriteLine("Exited");
                break;
        }


    } while (choice != 5);
}
catch (Exception e)
{
    System.Console.WriteLine(e.Message);
}