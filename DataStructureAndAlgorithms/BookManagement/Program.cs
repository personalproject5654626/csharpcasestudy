﻿void getMenu()
{
    System.Console.WriteLine("----------------------------");
    System.Console.WriteLine("|1. Add book into stack    |");
    System.Console.WriteLine("|2. Remove book from stack |");
    System.Console.WriteLine("|3. View book at top       ");
    System.Console.WriteLine("|4. Count book ");
    System.Console.WriteLine("|5. Exit");
}
int getChoice()
{
    try
    {
        System.Console.Write("Enter chocie: ");
        int chocie = int.Parse(Console.ReadLine()!);
        return chocie;
    }
    catch
    {
        throw new Exception("Your choice out of Range!");
    }
}

try
{
    int chocie;
    BookManagement bookManagement = new BookManagement(5);
    do
    {
        getMenu();
        chocie = getChoice();
        switch (chocie)
        {
            case 1:
                bookManagement.AddBook(new Book("Hoa Hoc", "Nhat Quang"));
                bookManagement.AddBook(new Book("Toan Hoc", "Nhat Quang"));
                bookManagement.AddBook(new Book("Sinh Hoc", "Nhat Quang"));
                bookManagement.AddBook(new Book("Vat Ly Hoc", "Nhat Quang"));
                break;
            case 2:
                var removed = bookManagement.RemoveBookAtTop();
                System.Console.WriteLine(removed);
                break;
            case 3:
                var printed = bookManagement.PrintBookAtTop();
                System.Console.WriteLine($"Tittle: {printed.Tittle} Author: {printed.Author}");
                break;
            case 4:
                var counted = bookManagement.CountElementInBook();
                System.Console.WriteLine($"Count book: {counted}");
                break;
            case 5:
                System.Console.WriteLine("Exit");
                break;
        }
    } while (chocie != 5);
}
catch (Exception e)
{
    System.Console.WriteLine($"Error: {e.Message}");
}