public class BookManagement
{
    int maxStack;
    Book[] books;
    int top = -1;

    public BookManagement(int maxStack)
    {
        this.maxStack = maxStack;
        books = new Book[maxStack];
    }
    public void AddBook(Book book)
    {
        if (IsFull())
        {
            throw new Exception("Stack is full");
        }
        else
        {
            top++;
            books[top] = new Book();
            books[top] = book;
        }
    }
    public Book RemoveBookAtTop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        var book = books[top];
        top--;
        return book;
    }
    public Book PrintBookAtTop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        return books[top];
    }
    public int CountElementInBook() => top + 1;

    public bool IsEmpty() => top == -1;

    public bool IsFull() => top == maxStack;
}