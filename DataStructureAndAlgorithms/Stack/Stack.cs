public class Stack
{
    public int[] items;
    public int maxStack;
    int top = -1;
    public Stack(int _maxStack)
    {
        maxStack = _maxStack;

        items = new int[maxStack];
    }

    public void Push(int item)
    {

        if (IsFull())
        {
            throw new Exception("Stack is full");
        }
        else
        {
            top++;
            items[top] = item;
        }
    }
    public int Pop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        else
        {
            int valueAtTop = items[top];
            top--;
            return valueAtTop;
        }
    }
    public int Top()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        else
        {
            return items[top];
        }
    }

    public bool IsEmpty()
    {
        if (top == -1)
        {
            return true;
        }
        return false;
    }
    public bool IsFull()
    {
        if (top == maxStack)
        {
            return true;
        }
        return false;
    }

}