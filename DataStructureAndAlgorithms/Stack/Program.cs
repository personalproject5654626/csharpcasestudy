﻿try
{
    Stack stack = new Stack(4);
    stack.Push(1);
    stack.Push(2);
    stack.Push(3);
    stack.Push(4);

    System.Console.WriteLine(stack.Pop());
    System.Console.WriteLine(stack.Pop());
    System.Console.WriteLine(stack.Pop());
    System.Console.WriteLine(stack.Pop());

}
catch (Exception ex)
{
    System.Console.WriteLine(ex.Message);
}