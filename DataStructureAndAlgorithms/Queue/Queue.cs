using System;
public class Queue
{
    int[] queue;
    int bot = -1;
    int top = -1;
    int maxQueue;
    public Queue(int maxQueue)
    {
        this.maxQueue = maxQueue;
        queue = new int[maxQueue];
    }
    public void EnQueue(int item)
    {
        if (IsEmpty())
        {
            top = 0; bot = 0;
            queue[bot] = item;
            return;
        }
        if (IsFull())
        {
            throw new Exception("Queue is full");
        }
        bot++;
        queue[bot] = item;
    }
    public int DeQueue()
    {
        if (IsEmpty())
        {
            throw new Exception("Queue is empty");
        }
        int valueAtTop = queue[top];
        if (top == bot)
        {
            top = -1;
            bot = -1;
        }
        else
        {
            top++;
        }
        return valueAtTop;
    }

    private bool IsFull() => bot - top + 1 >= maxQueue;
    private bool IsEmpty() => top == -1 && bot == -1;
    int EmptyQueue() => bot = top = -1;
}