﻿Queue queue = new Queue(5);

queue.EnQueue(1);
queue.EnQueue(2);
queue.EnQueue(3);
queue.EnQueue(4);
queue.EnQueue(5);

System.Console.WriteLine($"DeQueue: {queue.DeQueue()}");
System.Console.WriteLine($"DeQueue: {queue.DeQueue()}");
System.Console.WriteLine($"DeQueue: {queue.DeQueue()}");
System.Console.WriteLine($"DeQueue: {queue.DeQueue()}");
System.Console.WriteLine($"DeQueue: {queue.DeQueue()}");