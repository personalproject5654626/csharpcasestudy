﻿
void GetMenu()
{
    Console.WriteLine("******** BANK MANAGEMENT *********");
    Console.WriteLine("1 | Add customer into queue              |");
    Console.WriteLine("2 | Call customer will out queue         |");
    Console.WriteLine("3 | Show List Customer Withdrawn         |");
    Console.WriteLine("4 | Show Total Amount Withdrawn          |");
    Console.WriteLine("5 | Show Total Remainder Amount Withdrawn|");
    Console.WriteLine("_____________________________________________");
}
int getChoice()
{
    try
    {
        Console.Write("Enter choice: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch
    {
        throw new Exception("Wrong of format");
    }
}


BankManagement bankManagement = new BankManagement(10);
int choice = 0;
do
{
    try
    {
        GetMenu();
        bankManagement.DisPlayQueue();
        choice = getChoice();
        switch (choice)
        {
            case 1:
                Customer customer = new Customer();
                bankManagement.CustomerInput(customer);
                bankManagement.AddCustomerToQueue(customer);
                break;
            case 2:
                var inforCustomer = bankManagement.CallNextCustomerWillOutQueue();
                System.Console.WriteLine($"Customer ID: {inforCustomer.CustomerId}" +
                                        $" | Customer Name: {inforCustomer.Name}");
                break;
            case 3:
                var nameCustomerWithDrawn = bankManagement.ShowListCustomerWithDrawn();
                Console.WriteLine($"Customer: {nameCustomerWithDrawn} withdrwan");
                break;
            case 4:
                var totalAmountWithDrawn = bankManagement.ShowTotalAmountWithdrawn();
                Console.WriteLine($"Total money withdrawn:  {totalAmountWithDrawn}");
                break;
            case 5:
                var totalAmountRemainderInQueue = bankManagement.TotalRemainderInQueue();
                Console.WriteLine($"Total money remainder in queue: {totalAmountRemainderInQueue}");
                break;
            case 6:
                Console.WriteLine("Thank you for using my service");
                break;
            default:
                Console.WriteLine("Enter again!");
                break;
        }
    }
    catch (Exception e)
    {
        System.Console.WriteLine(e.Message);
    }
} while (choice != 6);



