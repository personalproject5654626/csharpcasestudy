using System.Text;

public class BankManagement
{
    public Customer[] customers;
    private int front = -1;
    private int rear = -1;
    private int maxQueue = 0;
    const int VIP_CUSTOMER = 1_000;
    const int FRIST_THREE_CUSTOMER = 3;

    public BankManagement(int maxQueue)
    {
        this.maxQueue = maxQueue;
        customers = new Customer[maxQueue];
    }
    public Customer CustomerInput(Customer customer)
    {
        Console.Write("Enter customer name: ");
        customer.Name = Console.ReadLine();
        Console.Write("Enter customer amout: ");
        customer.AmountToWithDraw = int.Parse(Console.ReadLine()!);
        customer.CustomerId = 0;
        return customer;
    }

    public void AddCustomerToQueue(Customer customer)
    {
        if (IsEmpty())
        {
            front = 0; rear = 0;
            IncreasementCustomerId(customer);
            customers[rear] = customer;
            return;
        }
        if (IsFull())
        {
            throw new Exception("Queue is full");
        }
        IncreasementCustomerId(customer);
        rear++;
        if (IsVipCustomer(customer))
        {
            for (int i = rear; i > front; i--)
            {
                customers[i] = customers[i - 1];
            }
            customers[front] = customer;
        }
        else
        {
            IncreasementCustomerId(customer);
            customers[rear] = customer;
        }
    }

    public Customer CallNextCustomerWillOutQueue()
    {
        if (IsEmpty())
        {
            throw new Exception("Queue is empty");
        }
        var customerAtFront = customers[front];
        if (front == rear)
        {
            front = 0;
            rear = -1;
        }
        else
        {
            front++;
        }
        return customerAtFront;
    }
    public string ShowFirstThreeCustomers()
    {
        if (IsEmpty())
        {
            throw new Exception("Queue is empty");
        }
        StringBuilder sb = new StringBuilder();
        int checkThreeCustomer = Math.Min(rear + 1, front + FRIST_THREE_CUSTOMER);
        for (int i = front; i < checkThreeCustomer; i++)
        {
            sb.Append($" {customers[i].CustomerId} | {customers[i].Name} \n").ToString();
        }
        return sb.ToString();
    }
    public void DisPlayQueue()
    {
        if (rear > -1)
        {
            Console.WriteLine("-------------------");
            Console.WriteLine(ShowFirstThreeCustomers());
            Console.WriteLine("\n-------------------");
        }
    }
    public string ShowListCustomerWithDrawn()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < front; i++)
        {
            sb.Append($"{customers[i].Name!.ToUpper()}");
        }
        return sb.ToString();
    }
    public int ShowTotalAmountWithdrawn()
    {
        int sum = 0;
        for (int i = 0; i < front; i++)
        {
            sum += customers[i].AmountToWithDraw;
        }
        return sum;
    }
    public int TotalRemainderInQueue()
    {
        int sum = 0;
        for (int i = front; i <= rear; i++)
        {
            sum += customers[i].AmountToWithDraw;
        }
        return sum;
    }
    private int IncreasementCustomerId(Customer customer) => customer.CustomerId = rear + 1;
    private bool IsVipCustomer(Customer customer) => customer.AmountToWithDraw >= VIP_CUSTOMER;
    private bool IsFull() => rear == maxQueue - 1;
    private bool IsEmpty() => rear == -1;
}