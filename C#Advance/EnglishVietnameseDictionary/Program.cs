﻿int choice = 0;
DictonaryManagement management = new DictonaryManagement();
Run();
void Run()
{
    do
    {
        try
        {

            GetMenuGeneral();
            choice = GetChoice();
            switch (choice)
            {
                case 1:
                    AddWord();
                    break;
                case 2:
                    FindAndUpdateWords();
                    break;
                case 3:
                    RemoveWord();
                    break;
                case 4:
                    DisplayWords();
                    break;
                case 5:
                    Console.WriteLine("Good Bye");
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    } while (choice != 5);
}
void AddWord()
{
    Console.Write("Enter english word: ");
    string englishWord = Console.ReadLine()!;
    Console.Write("Enter Vietnamese word meaning: ");
    string vietnameseMeaning = Console.ReadLine()!;
    var addedWord = management.AddWordToDictionary(englishWord, vietnameseMeaning);
    if (addedWord is true)
    {
        Console.WriteLine("Word is added into Dictonary");
    }
    else
    {
        Console.WriteLine("Word is not added into Dictonary");
    }
}
void FindAndUpdateWords()
{
    Console.Write("Enter word to find in Dictionary: ");
    string searchWord = Console.ReadLine()!;
    var foundWord = management.SearchWord(searchWord);
    Console.WriteLine($"English: {foundWord.EnglishWords} - Vietnamese: {foundWord.VietnameseWords}");
    if (foundWord is not null)
    {
        Console.WriteLine("Are you wana change word(Y/N): ");
        string answer = Console.ReadLine()!.ToLower();
        if (answer.Equals("y"))
        {
            Console.WriteLine($"Enter new meaning for {foundWord.VietnameseWords}: ");
            string newMeaning = Console.ReadLine()!;
            var updatedWord = management.UpdateWord(searchWord, newMeaning);
            if (updatedWord is true)
            {
                Console.WriteLine($"Word is update is {newMeaning}");
            }
            else
            {
                Console.WriteLine("Update word failed");
            }
        }
    }
}

void RemoveWord()
{
    Console.Write("Enter word to remove:");
    string removeWord = Console.ReadLine()!;
    var removedWord = management.RemoveWord(removeWord);
    if (removedWord)
    {
        Console.WriteLine("Word removed successfully");
    }
    else
    {
        Console.WriteLine("Remove word failed");
    }
}
void DisplayWords()
{
    int options = 0;
    do
    {
        GetDisplayMenu();
        options = GetChoice();
        switch (options)
        {
            case 1:
                var allWords = management.GetAllWords();

                Console.WriteLine("English      |      VietNamese");
                Console.WriteLine("--------------------------------------");

                foreach (Word word in allWords)
                {
                    string formattedEnglish = $"{word.EnglishWords,-12}";
                    string formattedVietnamese = $"{word.VietnameseWords, 7}";

                    string formattedLine = $"{formattedEnglish} | {formattedVietnamese}";
                    Console.WriteLine(formattedLine);
                }
                break;
            case 2:
                var englishWords = management.GetAllWords();
                foreach (Word word in englishWords)
                {
                    Console.WriteLine($"English: {word.EnglishWords}");
                }
                break;
            case 3:
                var VietnameseWords = management.GetAllWords();
                foreach (Word word in VietnameseWords)
                {
                    Console.WriteLine($"English: {word.VietnameseWords}");
                }
                break;
            case 4:
                Run();
                break;
            default:
                break;
        }
    } while (options != 4);
}
void GetMenuGeneral()
{
    Console.WriteLine("----------------------------------------");
    Console.WriteLine("1| Add new word into Dictionary");
    Console.WriteLine("2| Find word in Dictionary");
    Console.WriteLine("3| Remove word in Dictionary");
    Console.WriteLine("4| Display all words in Dictionary");
    Console.WriteLine("5| Exit");
    Console.WriteLine("----------------------------------------");
}
void GetDisplayMenu()
{
    Console.WriteLine("----------------------------------------");
    Console.WriteLine("1| Show all words in Dictionary");
    Console.WriteLine("2| Show only English word in Dictionary");
    Console.WriteLine("3| Show only Vietnamese word in Dictionary");
    Console.WriteLine("4| Back to Menu");
    Console.WriteLine("----------------------------------------");
}
int GetChoice()
{
    try
    {
        Console.Write("Enter your chocie: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (FormatException)
    {
        Console.WriteLine("Invalid chocie, Please enter a valid choice");
        return GetChoice();
    }
}


