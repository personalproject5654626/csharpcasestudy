public class Word
{
    private string englishWords =null!;
    private string vietnameseWords = null!;

    public Word(string _englishWords, string _vietnameseWords)
    {
        EnglishWords = _englishWords;
        VietnameseWords = _vietnameseWords;
    }
    public string EnglishWords {get => englishWords; set=>englishWords = value; }
    public string VietnameseWords {get =>vietnameseWords; set=>vietnameseWords = value; }
    
}