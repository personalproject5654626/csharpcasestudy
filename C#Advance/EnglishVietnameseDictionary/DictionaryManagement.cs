public class DictonaryManagement
{
    Dictionary<string, Word> dictionary;

    public DictonaryManagement()
    {
        dictionary = new Dictionary<string, Word>();
    }

    public bool AddWordToDictionary(string englishWord, string vietnameseMeaning)
    {
        string key = englishWord.ToLower();
        if (dictionary.ContainsKey(key))
        {
            return false;
        }
        Word word = new Word(englishWord, vietnameseMeaning);
        dictionary.Add(key, word);
        return true;
    }
    public Word SearchWord(string searchWord)
    {
        string key = searchWord.ToLower();
        bool foundWord = dictionary.TryGetValue(key, out Word? word);
        if (foundWord)
        {
            return word!;
        }
        return null!;
    }
    public bool UpdateWord(string searchWord, string newMeaningWord)
    {
        string searchWordKey = searchWord.ToLower();
        bool foundWord = dictionary.TryGetValue(searchWordKey, out Word? word);
        if (foundWord)
        {
            word!.VietnameseWords = newMeaningWord;
            return true;
        }
        return false;
    }
    public bool RemoveWord(string searchWord)
    {
        string key = searchWord.ToLower();
        bool foundWord = dictionary.TryGetValue(key, out Word? word);
        if (foundWord)
        {
            dictionary.Remove(key);
            return true;
        }
        return false;
    }
    public List<Word> GetAllWords() => new List<Word>(dictionary.Values);

}