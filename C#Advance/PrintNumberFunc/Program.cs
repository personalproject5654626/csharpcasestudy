﻿int[] numbers = {-1,2,3,0,-3,4,-2};
Func<int, int> function = Print;
var result =PrintNumber(numbers, function);
foreach(var number in result){
    Console.WriteLine(number);
}

int[] PrintNumber(int[] numbers, Func<int, int> function){
    int[] result = new int[numbers.Length];
    for(int i = 0; i < numbers.Length; i++){
        result[i] = function(numbers[i]);
    }
    return result;
}
int Print(int number){
    if(number > 0){
        return number * number;
    }
    else if(number < 0){
        return Math.Abs(number);
    }
    return 0;
}