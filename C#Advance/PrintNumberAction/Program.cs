﻿int[] numbers = {-1,0,9,6,-4};
Action<int> act = Print;
PrintNumerics(numbers, act);

void PrintNumerics(int[] numbers, Action<int> action){
    foreach(int number in numbers){
        action(number);
    }
}

void Print(int number){
    if(number < 0){
        Console.WriteLine("Negative");
    }else if(number > 0){
        Console.WriteLine("Positive");
    }else{
        Console.WriteLine("Zero");
    }
}