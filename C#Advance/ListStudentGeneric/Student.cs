public class Student
{
    private string name = null!;
    private int age;
    private double gpa;

    public Student()
    {
    }

    public Student(string name, int age, double gpa)
    {
        Name = name;
        Age = age;
        GPA = gpa;
    }

    public string Name
    {
        get => name;
        set => name = value;
    }
    public int Age
    {
        get => age;
        set
        {
            if (value < 0 || value > 100)
            {
                throw new ArgumentOutOfRangeException();
            }
            age =value;
        }
    }
    public double GPA
    {
        get => gpa;
        set
        {
            if (value < 0.0 || value > 10.0)
            {
                throw new ArgumentOutOfRangeException();
            }
            gpa = value;
        }
    }
}