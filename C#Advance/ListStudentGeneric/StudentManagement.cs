using System.Text;

public class StudentManagement
{
    List<Student> students;
    public StudentManagement()
    {
        students = new List<Student>();
    }

    public void AddStudent(Student student)
    {
        students.Add(student);
    }
    public bool RemoveStudentByName(string name)
    {
        foreach (Student student in students)
        {
            if (student.Name.Equals(name))
            {
                students.Remove(student);
                return true;
            }
        }
        return false;
    }

    public string DisplayStudent()
    {
        if(IsEmpty()){
            throw new InvalidOperationException("List is empty");
        }
        StringBuilder sb = new StringBuilder();
        foreach (Student student in students)
        {
            sb.Append($"Name: {student.Name} | Age: {student.Age} | GPA: {student.GPA}\n");
        }
        return sb.ToString();
    }
    public Student FindStudentByName(string name)
    {
        Student foundStudent = new Student();
        foreach (Student student in students)
        {
            if (student.Name.ToLower().Equals(name.ToLower()))
            {
                foundStudent = student;
            }
        }
        return foundStudent;
    }
    private bool IsEmpty() => students.Count == 0;
}