﻿void GetMenu()
{
    Console.WriteLine("--------------------------------------");
    Console.WriteLine("1| Add Student into List");
    Console.WriteLine("2| Remove Student by Name from List");
    Console.WriteLine("3| Display Student from List");
    Console.WriteLine("4| Find Student by Name from List");
    Console.WriteLine("5| Exit");
    Console.WriteLine("--------------------------------------");
}

int GetChoice()
{
    try
    {
        Console.Write("Enter your chocie: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (FormatException)
    {
        Console.WriteLine("Invalid input. Please try again.");
        return GetChoice();
    }
}
Student InputStudent()
{
    Console.Write("Enter name Student: ");
    string name = Console.ReadLine()!;
    Console.Write("Enter age Student: ");
    int age = int.Parse(Console.ReadLine()!);
    Console.Write("Enter GPA Student: ");
    double gpa = double.Parse(Console.ReadLine()!);
    Student student = new Student(name, age, gpa);
    return student;
}
string InputName()
{
    Console.WriteLine("Enter name Student: ");
    string name = Console.ReadLine()!;
    return name;
}

StudentManagement manager = new StudentManagement();
int choice = 0;
do
{
   try
   {
     GetMenu();
    choice = GetChoice();
    switch (choice)
    {
        case 1:
            Student student = InputStudent();
            manager.AddStudent(student);
            break;
        case 2:
            string name = InputName();
           var removedStudent= manager.RemoveStudentByName(name);
           if(removedStudent){
            Console.WriteLine($"Removed student successfully");
           }else{
            Console.WriteLine("Student Name is not found");
           }
            break;
        case 3:
            var displayStudent = manager.DisplayStudent();
            Console.WriteLine("List Student");
            Console.WriteLine(displayStudent);
            break;
        case 4:
            string findName = InputName();
            var foundStudent = manager.FindStudentByName(findName);
            Console.WriteLine($"Name: {foundStudent.Name} | Age: {foundStudent.Age} | GPA: {foundStudent.GPA}");
            break;
        case 5:
            Console.WriteLine("See you soon");
            break;
        default:
            break;
    }
   }
   catch (System.Exception ex)
   {
     Console.WriteLine($"Error: {ex.Message}");
   }
} while (choice != 5);

