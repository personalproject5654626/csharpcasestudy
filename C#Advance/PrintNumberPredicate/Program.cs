﻿var words = new List<string> { "apple", "banana", "orange", "lemon" };
Predicate<string> predicate = CheckWord;
var result = PrintWords(words, predicate);
foreach(var word in result){
    Console.WriteLine(word);
}

List<string> PrintWords(List<string> words, Predicate<string> predicate)
{
    var result = new List<string>();
    foreach (var word in words)
    {
        if (predicate(word))
         result.Add(word);
    }
    return result;
}
bool CheckWord(string word) => word.Length >= 6;