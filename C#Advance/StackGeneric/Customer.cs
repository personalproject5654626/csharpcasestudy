struct Customer
{
    private int id;
    private string name = null!;
    public int Id { get => id; set => id = value; }
    public string Name { get => name; set => name = value; }
    public Customer(int _id, string _name)
    {
        Id = _id;
        Name = _name;
    }
}