public class Person
{
    private string name = null!;
    private int phoneNumber;
    public string Name { get => name; set => name = value;}
    public int PhoneNumber { get => phoneNumber; set => phoneNumber = value;}
    public Person(string _name, int _phoneNumber)
    {
        Name = _name;
        PhoneNumber = _phoneNumber;
    }
}