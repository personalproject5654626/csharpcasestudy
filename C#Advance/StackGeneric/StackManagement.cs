class StackManagement
{
    private Stack<int> intStack;
    private Stack<string> stringStack;
    private Stack<Person> personStack;
    private Stack<Customer> customerStack;

    public StackManagement(int maxStack)
    {
        intStack = new Stack<int>(maxStack);
        stringStack = new Stack<string>(maxStack);
        personStack = new Stack<Person>(maxStack);
        customerStack = new Stack<Customer>(maxStack);
    }

    public void Run()
    {
        while (true)
        {
            RunMenu();
            int option = GetChoice();
            switch (option)
            {
                case 1:
                    while (true)
                    {
                        StackMenu();
                        int choiceInteger = GetChoice();
                        switch (choiceInteger)
                        {
                            case 1:
                                PushInteger();
                                break;
                            case 2:
                                PopIntegers();
                                break;
                            case 3:
                                PeekInteger();
                                break;
                            case 4:
                                CounInteger();
                                break;
                            case 5:
                                GetLastItemIndexInteger();
                                break;
                            default:
                                Run();
                                break;
                        }
                    }

                case 2:
                    while (true)
                    {
                        StackMenu();
                        int choiceString = GetChoice();
                        switch (choiceString)
                        {
                            case 1:
                                PushString();
                                break;
                            case 2:
                                PopString();
                                break;
                            case 3:
                                PeekString();
                                break;
                            case 4:
                                CountString();
                                break;
                            case 5:
                                GetLastItemIndexString();
                                break;
                            default:
                                Run();
                                break;
                        }
                    }
                case 3:
                    while (true)
                    {
                        StackMenu();
                        int choicePerson = GetChoice();
                        switch (choicePerson)
                        {
                            case 1:
                                PushPerson();
                                break;
                            case 2:
                                PopPerson();
                                break;
                            case 3:
                                PeekPerson();
                                break;
                            case 4:
                                CountPerson();
                                break;
                            case 5:
                                GetLastItemIndexPerson();
                                break;
                            default:
                                Run();
                                break;
                        }
                    }

                case 4:
                    while (true)
                    {
                        StackMenu();
                        int choiceCustomer = GetChoice();
                        switch (choiceCustomer)
                        {
                            case 1:
                                PushCustomer();
                                break;
                            case 2:
                                PopCustomer();
                                break;
                            case 3:
                                PeekCustomer();
                                break;
                            case 4:
                                CountCustomer();
                                break;
                            case 5:
                                GetLastItemIndexCustomer();
                                break;
                            default:
                                Run();
                                break;
                        }
                    }
                default:
                    Environment.Exit(0);
                    break;
            }
        }
    }
    private void PushInteger()
    {
        Console.Write("Enter integer to push: ");
        int pushInteger = int.Parse(Console.ReadLine()!);
        try
        {
            intStack.Push(pushInteger);
            Console.WriteLine("Integer push successfully");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
    private void PushString()
    {
        Console.Write("Enter string to push: ");
        string pushString = Console.ReadLine()!;
        try
        {
            stringStack.Push(pushString);
            Console.WriteLine("String push successfully");
        }
        catch (System.Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
    private void PushPerson()
    {
        Console.Write("Enter person name: ");
        string name = Console.ReadLine()!;
        Console.Write("Enter person phone number: ");
        int phoneNumber = int.Parse(Console.ReadLine()!);
        Person person = new Person(name, phoneNumber);
        try
        {
            personStack.Push(person);
            Console.WriteLine("Person push successfully");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
    private void PushCustomer()
    {
        Console.Write("Enter customer id: ");
        int id = int.Parse(Console.ReadLine()!);
        Console.Write("Enter customer name: ");
        string name = Console.ReadLine()!;
        Customer customer = new Customer(id, name);
        try
        {
            customerStack.Push(customer);
            Console.WriteLine("Customer push successfully");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
    private void PopIntegers()
    {
        try
        {
            int popedIntegers = intStack.Pop();
            Console.WriteLine($"Poped Integer: {popedIntegers}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PopString()
    {
        try
        {
            string popedString = stringStack.Pop();
            Console.WriteLine($"Poped String: {popedString}");
        }
        catch (System.Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PopPerson()
    {
        try
        {
            Person popedPersons = personStack.Pop();
            Console.WriteLine($"Poped Person Name: {popedPersons.Name}");
            Console.WriteLine($"Poped Person Phone Number: {popedPersons.PhoneNumber}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PopCustomer()
    {
        try
        {
            Customer popedCustomer = customerStack.Pop();
            Console.WriteLine($"Poped Customer Id: {popedCustomer.Id}");
            Console.WriteLine($"Poped Custoemr Name: {popedCustomer.Name}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PeekInteger()
    {
        try
        {
            int peekInteger = intStack.Peek();
            Console.WriteLine($"Peek Integer: {peekInteger}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PeekString()
    {
        try
        {
            string peekString = stringStack.Peek();
            Console.WriteLine($"Peek String: {peekString}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PeekPerson()
    {
        try
        {
            var peekPerson = personStack.Peek();
            Console.WriteLine($"Peek Person| Name: {peekPerson.Name} - Phone Number: {peekPerson.PhoneNumber}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void PeekCustomer()
    {
        try
        {
            var peekCustomer = customerStack.Peek();
            Console.WriteLine($"Peek Customer| Id: {peekCustomer.Id} - Name: {peekCustomer.Name}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void CounInteger()
    {
        try
        {
            var countInteger = intStack.Count();
            Console.WriteLine($"Count Integer: {countInteger}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void CountString()
    {
        try
        {
            var countString = stringStack.Count();
            Console.WriteLine($"Count String: {countString}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void CountPerson()
    {
        try
        {
            var countPerson = personStack.Count();
            Console.WriteLine($"Count Person: {countPerson}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void CountCustomer()
    {
        try
        {
            var countCustomer = customerStack.Count();
            Console.WriteLine($"Count Customer: {countCustomer}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void GetLastItemIndexPerson()
    {
        try
        {
            var getLastItemIndexPerson = personStack.GetLastItemIndex();
            Console.WriteLine($"Get Last Item Index Person| Name:{getLastItemIndexPerson.Name} - Phone Number: {getLastItemIndexPerson.PhoneNumber}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void GetLastItemIndexCustomer()
    {
        try
        {
            var getLastItemIndexCustomer = customerStack.GetLastItemIndex();
            Console.WriteLine($"Get Last Item Index Customer| Id: {getLastItemIndexCustomer.Id} - Name: {getLastItemIndexCustomer.Name}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void GetLastItemIndexString()
    {
        try
        {
            var getLastItemIndexString = stringStack.GetLastItemIndex();
            Console.WriteLine($"Get Last Item Index String: {getLastItemIndexString}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    private void GetLastItemIndexInteger()
    {
        try
        {
            var getLastItemIndexInteger = intStack.GetLastItemIndex();
            Console.WriteLine($"Get Last Item Index String: {getLastItemIndexInteger}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    private void RunMenu()
    {
        Console.WriteLine("-----------------------------------");
        Console.WriteLine("1| Stack with Int");
        Console.WriteLine("2| Stack with String");
        Console.WriteLine("3| Stack with Class");
        Console.WriteLine("4| Stack with Struct");
        Console.WriteLine("5| Exit");
        Console.WriteLine("-----------------------------------");
    }
    private void StackMenu()
    {
        Console.WriteLine("-----------------------------------");
        Console.WriteLine("1| Push item into Stack");
        Console.WriteLine("2| Pop item from Stack");
        Console.WriteLine("3| Peek item from Stack");
        Console.WriteLine("4| Count item in Stack");
        Console.WriteLine("5| Get last item index in Stack");
        Console.WriteLine("6| Back to menu");
        Console.WriteLine("-----------------------------------");
    }
    private int GetChoice()
    {
        try
        {
            Console.Write("Enter your choice: ");
            int chocie = int.Parse(Console.ReadLine()!);
            return chocie;
        }
        catch (FormatException)
        {
            Console.WriteLine("Invalid input. Please enter a valid integer choice.");
            return GetChoice();
        }
    }

}