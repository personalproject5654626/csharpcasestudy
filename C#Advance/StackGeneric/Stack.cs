public class Stack<T>
{
    T[] items;
    int top = -1;
    int maxStack;
    public Stack(int _maxStack)
    {
        maxStack = _maxStack;
        items = new T[maxStack];
    }

    public void Push(T item)
    {
        if (IsFull())
        {
            throw new Exception("Stack overflow");
        }
        top++;
        items[top] = item;
    }
    public T Pop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack empty");
        }
        var valueAtTop = items[top];
        top--;
        return valueAtTop;
    }
    public T Peek(){
        if(IsEmpty()){
            throw new Exception("Stack empty");
        }
        return items[top];
    }
    public int Count() => top + 1;

    public T GetLastItemIndex(){
        if(IsEmpty()){
            throw new Exception("Stack empty");
        }
        return items[items.Length - 1];
    }
    
    private bool IsEmpty() => top == -1;
    private bool IsFull() => top == maxStack;
}