using Newtonsoft.Json;

public class TodoManagement
{
    private const string JsonFilePath = "todos.json";
    private List<Todo> todos;

    public TodoManagement()
    {
        todos = new List<Todo>();
    }
    public void LoadData()
    {
        if (File.Exists(JsonFilePath))
        {
            string json = File.ReadAllText(JsonFilePath);
            todos = JsonConvert.DeserializeObject<List<Todo>>(json)!;
        }
        else
        {
            todos = new List<Todo>();
        }
    }
    public void SaveData()
    {
        string json = JsonConvert.SerializeObject(todos);
        File.WriteAllText(JsonFilePath, json);
    }

    public bool AddNewTask(string newTask)
    {
        int newId = todos.Any() ? todos.Max(t => t.id) + 1 : 1;
        Todo newTodo = new Todo(DateTime.Now, newTask, false, newId);
        if (newTodo is not null)
        {
            todos.Add(newTodo);
            SaveData();
            return true;
        }
        return false;
    }
    public bool RemoveTodoById(int idRemove)
    {
        Todo todoRemove = null!;
        foreach (var todo in todos)
        {
            if (todo.id == idRemove)
            {
                todoRemove = todo;
                break;
            }
        }
        if (todoRemove is not null)
        {
            todos.Remove(todoRemove);
            SaveData();
            return true;
        }
        return false;
    }
    public bool MarkTodoAsCompleted(int idMark)
    {
        Todo todoMark = null!;
        foreach (var todo in todos)
        {
            if (todo.id == idMark)
            {
                todoMark = todo;
                break;
            }
        }
        if (todoMark is not null)
        {
            todoMark.IsCompleted = true;
            SaveData();
            return true;
        }
        return false;
    }
    public void DisplayTodo(){
        if(todos.Any()){
            foreach(var todo in todos){
                string status = todo.IsCompleted ? "Completed" : "Not Completed";
                Console.WriteLine($"{todo.id} - {todo.taskTitle} | {status}");
            }
        }else{
            Console.WriteLine("No tasks");
        }
    }
}