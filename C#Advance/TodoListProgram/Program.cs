﻿TodoManagement management = new TodoManagement();
Run();
void Run() {
    int choice = 0;
    do
    {
        management.LoadData();
        try
        {
            GetMenu();
            choice = GetChoice();
            switch (choice){
                case 1:
                AddNewTodo();
                break;
                case 2:
                RemoveTodo();
                break;
                case 3:
                MarkTodoAsCompleted();
                break;
                case 4:
                Console.WriteLine("Good Bye");
                break;
                default:
                break;
            }
        }
        catch (Exception ex)
        {
             Console.WriteLine($"Error: {ex.Message}");
        }
    } while (choice != 4);
}
void AddNewTodo(){
    Console.WriteLine("Enter new Task: ");
    string newTask = Console.ReadLine()!;
    var addedTask = management.AddNewTask(newTask);
    if(addedTask is true){
        Console.WriteLine("Add Task successfully");
    }
    else{
        Console.WriteLine("Add Task failed");
    }
}
void RemoveTodo(){
    Console.Write("Enter TodoId to remove: ");
    int removeTaskId =int.Parse(Console.ReadLine()!);
    var removedTask = management.RemoveTodoById(removeTaskId);
    if(removedTask is true){
        Console.WriteLine("Task deleted successfully");
    }
    else{
        Console.WriteLine("Task deleted failed");
    }
}
void MarkTodoAsCompleted(){
    Console.Write("Enter TaskId mark comppleted: ");
    int markTodoId = int.Parse(Console.ReadLine()!);
    var markCompleted = management.MarkTodoAsCompleted(markTodoId);
    if(markCompleted is true){
        Console.WriteLine("Todo is completed");
    }else{
        Console.WriteLine("Todo is not completed");
    }
}
void GetMenu(){
    Console.WriteLine("***** TODO LIST ****");
    management.DisplayTodo();
    Console.WriteLine("***** Function *****");
    Console.WriteLine("1| Add new todo");
    Console.WriteLine("2| Remove todo");
    Console.WriteLine("3| Mark todo is completed");
    Console.WriteLine("4| Exit");
    Console.WriteLine("--------------------------------");
}
int GetChoice(){
    try
    {
        Console.Write("Enter your choice: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (FormatException)
    {
         Console.WriteLine("Invalid choice. Please try again.");
         return GetChoice();
    }
}