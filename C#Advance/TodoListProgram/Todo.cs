public class Todo
{
public Todo(DateTime _createAt, string _task, bool _isCompleted, int _id)
{
    createAt = _createAt;
    taskTitle = _task;
    IsCompleted = _isCompleted;
    id = _id;
}

    public DateTime createAt { get; set; }
    public string taskTitle { get; set; } = null!;
    public bool IsCompleted { get; set; }
    public int id { get; set; }
  
}