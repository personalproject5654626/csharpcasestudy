﻿MathOperation addOperation = new MathOperation(Addition);
MathOperation subOperation = new MathOperation(Subtraction);
MathOperation divOperation = new MathOperation(Divide);
MathOperation multiOperation = new MathOperation(Mutiplication);
Console.WriteLine(PerformOperation(3,4,addOperation));
Console.WriteLine(PerformOperation(3,4,subOperation));
Console.WriteLine(PerformOperation(3,4,divOperation));
Console.WriteLine(PerformOperation(3,4,multiOperation));


int PerformOperation(int x, int y, MathOperation mathOperation)
{
    return mathOperation(x, y);
}

int Addition(int a, int b) => a + b;
int Subtraction(int a, int b) => a - b;
int Divide(int a, int b) => a / b;
int Mutiplication(int a, int b) => a *= b;
delegate int MathOperation(int a, int b);