public class StudentManagement
{
    Dictionary<string, Student> dictionary;
    public StudentManagement()
    {
        dictionary = new Dictionary<string, Student>();
    }

    public bool AddStudent(string name, double score)
    {
        string key = name.ToLower();
        bool isExist = dictionary.ContainsKey(key);
        if (isExist)
        {
            return false;
        }
        Student student = new Student(name, score);
        dictionary.Add(key, student);
        return true;
    }
    public Student FindStudentByName(string searchName)
    {
        string searchNameKey = searchName.ToLower();
        bool foundStudent = dictionary.TryGetValue(searchNameKey, out Student? student);
        if (foundStudent is true)
        {
            return student!;
        }
        return null!;
    }
    public List<Student> GetAllStudent() => new List<Student>(dictionary.Values);
}