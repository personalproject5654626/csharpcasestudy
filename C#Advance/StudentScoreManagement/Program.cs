﻿StudentManagement management = new StudentManagement();
Run();
void Run()
{
    int choice = 0;
    do
    {
        try
        {
            GetMenu();
            choice = GetChoice();
            switch (choice)
            {
                case 1:
                    AddNewStudent();
                    break;
                case 2:
                    FindStudentByName();
                    break;
                case 3:
                    DisplayAllStudent();
                    break;
                case 4:
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    } while (choice != 4);
}
void AddNewStudent()
{
    var inforStudent = InputInfoStudent();
    var addedStudent = management.AddStudent(inforStudent.Name, inforStudent.Score);
    if (addedStudent is true)
    {
        Console.WriteLine("Add student successfully");
    }
    else
    {
        Console.WriteLine("Add student failed");
    }
}
void FindStudentByName()
{
    Console.Write("Enter name student wana find: ");
    string nameSearch = Console.ReadLine()!;
    var foundStudent = management.FindStudentByName(nameSearch);
    if (foundStudent is not null)
    {
        Console.WriteLine($"Name: {foundStudent.Name}- Score: {foundStudent.Score}");
    }
    else
    {
        Console.WriteLine("Student is not found");
    }
}
void DisplayAllStudent(){
    var allStudent = management.GetAllStudent();
    foreach(Student student in allStudent){
        Console.WriteLine($"Name: {student.Name}- Score: {student.Score}");
    }
}
Student InputInfoStudent()
{
    Console.Write("Enter name student: ");
    string name = Console.ReadLine()!;
    Console.Write("Enter score student: ");
    double score = double.Parse(Console.ReadLine()!);
    Student student = new Student(name, score);
    return student;
}

int GetChoice()
{
    try
    {
        Console.Write("Enter your choice: ");
        int choice = int.Parse(Console.ReadLine()!);
        return choice;
    }
    catch (FormatException)
    {
        Console.WriteLine("Invalid chocie. Please try again");
        return GetChoice();
    }
}

void GetMenu()
{
    Console.WriteLine("1| Add new student");
    Console.WriteLine("2| Find student by name");
    Console.WriteLine("3| Display all students");
    Console.WriteLine("4| Exit");
}