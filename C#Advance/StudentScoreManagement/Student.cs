
public class Student
{
    private string name = null!;
    private double score = 0;
    public string Name { get => name; set => name = value; }
    public double Score
    {
        get => score;
        set
        {
            if (value < 0 || value > 10.0)
            {
                throw new ArgumentException("Score out of range");
            }
            score = value;
        }
    }
    public Student()
    {

    }
    public Student(string _name, double _score)
    {
        Name = _name;
        Score = _score;
    }
}
