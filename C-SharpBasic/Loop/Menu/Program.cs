﻿
const string TITLE = "**** MENU PROGRAM ****";
const string OPTION_1 = "1. Sum two number";
const string OPTION_2 = "2. Cube of number";
const string OPTION_3 = "3. EXIT";
const string LINE = "------------------------------";
int choice = 0;

do
{
    //content
    System.Console.WriteLine(TITLE);
    System.Console.WriteLine(OPTION_1);
    System.Console.WriteLine(OPTION_2);
    System.Console.WriteLine(OPTION_3);
    System.Console.WriteLine(LINE);

    System.Console.WriteLine("Enter your choice: ");
    choice = int.Parse(Console.ReadLine()!);

    switch (choice)
    {
        case 1:
            System.Console.WriteLine("Enter two number to sum !");
            System.Console.WriteLine("Enter first number: ");
            int firstNumber = int.Parse(Console.ReadLine()!);
            System.Console.WriteLine("Enter second number: ");
            int secondNumber = int.Parse(Console.ReadLine()!);
            int sumNumber = firstNumber + secondNumber;
            System.Console.WriteLine($"Sum of two number: {sumNumber}");
            System.Console.WriteLine(LINE);
            break;
        case 2:
            System.Console.WriteLine("Enter num to cube :");
            System.Console.WriteLine("Number : ");
            double cubeNumber = int.Parse(Console.ReadLine()!);
            double result = Math.Pow(cubeNumber, 3);
            System.Console.WriteLine($"Cube is {result}");
            System.Console.WriteLine(LINE);
            break;
        case 3:
            System.Console.WriteLine("Menu Program was exit!");
            break;
        default:
            System.Console.WriteLine("Please enter again!");
            break;
    }

} while (choice != 3);
// Console.ReadLine();