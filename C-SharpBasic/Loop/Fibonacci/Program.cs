﻿System.Console.WriteLine("Enter integer number: ");
int number = int.Parse(Console.ReadLine()!);

int firstNumber = 0;
int secondNumber = 1;

for (int i = 0; i < number; i++)
{
    int nextNumber = firstNumber + secondNumber;
    System.Console.Write($"{firstNumber} ");
    firstNumber = secondNumber;
    secondNumber = nextNumber;
}