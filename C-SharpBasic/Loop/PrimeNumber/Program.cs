﻿System.Console.WriteLine("Enter number: ");
int number = int.Parse(Console.ReadLine()!);

for (int i = 2; i <= number; i++)
{
    bool flag = true;
    for (int j = 2; j < i; j++)
    {
        if (i % j == 0)
        {
            flag = false;
            break;
        }
    }
    if (flag)
    {
        System.Console.Write($"{i} ");
    }
}