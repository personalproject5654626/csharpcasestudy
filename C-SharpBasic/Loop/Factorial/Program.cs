﻿System.Console.WriteLine("Enter positive integer number : ");
int number = int.Parse(Console.ReadLine()!);
const int minNumber = 1;

if (number < 0)
{
    System.Console.WriteLine("Plese enter postive number!");
}
else
{
    System.Console.WriteLine("Enter you choice: ");
    string choice = Console.ReadLine()!;

    switch (choice)
    {
        case "while":
            int factories = 1;
            while (number >= minNumber)
            {
                factories *= number;
                if (number == 1)
                {
                    System.Console.Write($"{number} ");
                }
                else
                {
                    System.Console.Write($"{number} * ");
                }
                number--;
            }
            System.Console.WriteLine($" = {factories}");
            break;
        case "do":
            factories = 1;
            do
            {
                factories *= number;
                if (number == 1)
                {
                    System.Console.Write($"{number} ");
                }
                else
                {
                    System.Console.Write($"{number} * ");
                }
                number--;
            } while (number >= minNumber);
            System.Console.WriteLine($" = {factories}");
            break;
        case "for":
            factories = 1;
            for (; number >= minNumber; number--)
            {
                factories *= number;
                if (number == 1)
                {
                    System.Console.Write($"{number} ");
                }
                else
                    System.Console.Write($"{number} * ");
            }
            System.Console.WriteLine($" = {factories}");
            break;
        default:
            System.Console.WriteLine("Please choice again!");
            break;
    }
}