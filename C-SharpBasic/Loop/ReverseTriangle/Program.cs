﻿System.Console.WriteLine("Enter height: ");
int height = int.Parse(Console.ReadLine()!);

for (int i = height; i >= 1; i--)
{
    for (int j = 1; j <= i; j++)
    {
        System.Console.Write("*");
    }
    System.Console.WriteLine();
}