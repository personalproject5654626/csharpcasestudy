﻿System.Console.WriteLine("Enter integer number: ");
int number = int.Parse(Console.ReadLine()!);

int startNumber = 1;
int sum = 0;

//while loop
while (startNumber <= number)
{
    sum += startNumber;
    startNumber++;
}
System.Console.WriteLine($"Sum 1 to {number} is {sum}");

//do while loop
int startNumberDoLoop = 1;
int sumDoLoop = 0;
do
{

    sumDoLoop += startNumberDoLoop;
    startNumberDoLoop++;

} while (startNumberDoLoop <= number);
System.Console.WriteLine($"Sum 1 to {number} is {sumDoLoop}");

//for loop
int sumForLoop = 0;
for (int startNumberForLoop = 1; startNumberForLoop <= number; startNumberForLoop++)
{

    sumForLoop += startNumberForLoop;
}
System.Console.WriteLine($"Sum 1 to {number} is {sumForLoop}");
