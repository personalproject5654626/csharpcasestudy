﻿
System.Console.WriteLine("Enter width: ");
int width = int.Parse(Console.ReadLine()!);

System.Console.WriteLine("Enter height: ");
int height = int.Parse(Console.ReadLine()!);

for (int i = 0; i < height; i++)
{
    for (int j = 0; j < width; j++)
    {
        System.Console.Write("* ");
    }
    System.Console.WriteLine();
}