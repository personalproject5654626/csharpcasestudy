﻿System.Console.WriteLine("Enter height: ");
int height = int.Parse(Console.ReadLine()!);

for (int i = 0; i < height; i++)
{
    for (int j = 0; j <= i; j++)
    {
        System.Console.Write("* ");
    }
    System.Console.WriteLine();
}