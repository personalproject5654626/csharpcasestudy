using UnitTestIntroduction;

namespace UnitTestIntroductionTests
{
    public class IntroductionToUnitTestTests
    {

        [Fact]
        public void GetDayOfWeek_ShouldReturnInvalid_WhenDayNumberIs0()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Invalid day number";
            var dayNumber = 0;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnMonday_WhenDayNumberIs1()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Monday";
            var dayNumber = 1;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnTuesday_WhenDayNumberIs2()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Tuesday";
            var dayNumber = 2;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnWednesday_WhenDayNumberIs3()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Wednesday";
            var dayNumber = 3;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnThursday_WhenDayNumberIs4()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Thursday";
            var dayNumber = 4;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnFriday_WhenDayNumberIs5()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Friday";
            var dayNumber = 5;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnSaturday_WhenDayNumberIs6()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Saturday";
            var dayNumber = 6;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDayOfWeek_ShouldReturnSunday_WhenDayNumberIs7()
        {
            // assert
            var obj = new IntroductionToUnitTest();
            var expected = "Sunday";
            var dayNumber = 7;

            // act
            var actual = obj.GetDayOfWeek(dayNumber);

            // arrage
            Assert.Equal(expected, actual);
        }
    }
}
