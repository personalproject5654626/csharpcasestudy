﻿using Games;

int getChoice()
{
    System.Console.WriteLine("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}
void getMenu()
{
    System.Console.WriteLine("**** Game ****");
    System.Console.WriteLine("1. Play Ore Game");
    System.Console.WriteLine("2. Play Keo Bua Bao Game");
    System.Console.WriteLine("3. Exit");
    System.Console.WriteLine("------------------------");
}

int choice;
do
{
    getMenu();
    choice = getChoice();
    Game game = new Game();
    switch (choice)
    {
        case 1:
            int quantity = game.getQuantity();
            game.OreGames(quantity);
            break;
        case 2:
            game.playerChoice();
            break;
        case 3:
            Console.WriteLine("Exit");
            break;
        default:
            break;

    }
} while (choice != 3);