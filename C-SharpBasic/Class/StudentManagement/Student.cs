using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagement
{
    public class Student
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }

        public static Student[] students = new Student[2];

        public Student()
        {
            this.Id = Id;
            this.Name = Name;
            this.Age = Age;
            this.GPA = GPA;
        }
        public Student(int Id, string Name, int Age, double GPA)
        {
            this.Id = Id;
            this.Name = Name;
            this.Age = Age;
            this.GPA = GPA;

        }
        public void ExistStudent()
        {
            students[0] = new Student();
            students[0].Id = 1;
            students[0].Name = "Diamond";
            students[0].Age = 22;
            students[0].GPA = 10;

            students[1] = new Student();
            students[1].Id = 2;
            students[1].Name = "Diamond day";
            students[1].Age = 23;
            students[1].GPA = 10;
        }
        public Student AddNewStudent()
        {
            int newElements = students.Length + 1;
            Array.Resize(ref students, students.Length + 1);
            Student newStudent = new Student();
            Console.WriteLine("Enter student id: ");
            newStudent.Id = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Enter student name: ");
            newStudent.Name = Console.ReadLine();
            Console.WriteLine("Enter student age: ");
            newStudent.Age = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Enter student gpa: ");
            newStudent.GPA = double.Parse(Console.ReadLine()!);
            return students[newElements - 1] = newStudent;
        }
        public void ShowInfomation()
        {
            foreach (Student student in students)
            {
                Console.WriteLine("Id: " + student.Id + "| Tên: " + student.Name + "| Tuổi: " + student.Age + "| GPA: " + student.GPA);
            }
        }
        public string Ranking(int studentId)
        {
            foreach (Student student in students)
            {
                if (student.Id == studentId)
                {
                    Console.WriteLine("Id: " + student.Id +
                     "| Tên: " + student.Name +
                    "| Tuổi: " + student.Age +
                     "| GPA: " + student.GPA);
                    string ranking = "";
                    bool good = GPA >= 8.0;
                    bool pretyGood = GPA >= 6.5 && GPA < 8.0;
                    bool medium = GPA >= 5.0 && GPA < 6.5;

                    if (good)
                    {
                        ranking = "Good";
                    }
                    else if (pretyGood)
                    {
                        ranking = "Prety Good";
                    }
                    else if (medium)
                    {
                        ranking = "Medium";
                    }
                    else
                    {
                        ranking = "Bad";
                    }

                    return ranking;
                }
            }
            return "Not Found";
        }
    }
}