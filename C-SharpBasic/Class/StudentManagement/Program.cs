﻿using System;
using StudentManagement;
Student student = new Student();
int getChoice()
{
    Console.WriteLine("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}

void getMenu()
{
    Console.WriteLine("******* Student Management ******");
    Console.WriteLine("1. Enter Student Information ");
    Console.WriteLine("2. Show Student Information ");
    Console.WriteLine("3. Show rank of Student ");
    Console.WriteLine("4. Exit Program ");
    Console.WriteLine("-------------------------------------");
}
int choice;
do
{
    student.ExistStudent();
    getMenu();
    choice = getChoice();
    switch (choice)
    {

        case 1:
            student.AddNewStudent();
            break;
        case 2:
            student.ShowInfomation();
            break;
        case 3:
            Console.WriteLine("Enter student id wana find: ");
            int id = int.Parse(Console.ReadLine()!);
            student.Ranking(id);
            break;
        case 4:
            break;
        default:
            break;
    }
} while (choice != 4);