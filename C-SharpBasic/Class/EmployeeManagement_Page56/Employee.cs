using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement_Page56
{
    public class Employee
    {

        public string? Name { get; set; }
        public int Age { get; set; }
        public string? Position { get; set; }
        public float Salary { get; set; }

        public static Employee[] employeeList = new Employee[2];

        public Employee()
        {

        }
        public Employee(string name, int age, string position, float salary)
        {
            Name = name;
            Age = age;
            Position = position;
            Salary = salary;

        }
        public void ExitsEmployee()
        {
            employeeList[0] = new Employee("Diamond", 22, "PM", 1000);
            employeeList[1] = new Employee("Diamond day", 23, "Dev1", 1000);

        }

        public void PrintEmployees()
        {
            foreach (Employee employee in employeeList)
            {
                Console.WriteLine("Name: " + employee.Name +
                                "| Age: " + employee.Age +
                                "| Position: " + employee.Position +
                                "| Salary: " + employee.Salary);
            }
        }
        public Employee AddNewEmployee()
        {
            int newEmployeeList = employeeList.Length + 1;
            Array.Resize(ref employeeList, employeeList.Length + 1);
            Employee newEmployee = new Employee();
            Console.WriteLine("Enter employee Name: ");
            newEmployee.Name = Console.ReadLine();
            Console.WriteLine("Enter employee Age: ");
            newEmployee.Age = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Enter employee Position: ");
            newEmployee.Position = Console.ReadLine();
            Console.WriteLine("Enter employee Salary: ");
            newEmployee.Salary = float.Parse(Console.ReadLine()!);
            return employeeList[newEmployeeList - 1] = newEmployee;
        }
        public void RemoveEmployee(string name)
        {

            for (int i = 0; i < employeeList.Length; i++)
            {
                if (employeeList[i].Name == name)
                {
                    employeeList[i] = null;
                    Array.Resize(ref employeeList, employeeList.Length - 1);
                }
            }
        }
    }
}