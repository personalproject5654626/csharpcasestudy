using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarManagement
{
    public class Car
    {
        public static Car[] cars = new Car[10];

        public string? Make { get; set; }
        public string? Model { get; set; }
        public int Year { get; set; }
        public string? Color { get; set; }
        public Car()
        {

        }
        public Car(string make, string model, int year, string color)
        {
            Make = make;
            Model = model;
            Year = year;
            Color = color;
        }
        public void ExistCars()
        {
            cars[0] = new Car("Ford", "Mustang", 2022, "Red");
            cars[1] = new Car("Toyota", "Camry", 2021, "White");
            cars[2] = new Car("Mercesdez-Benz", "I8", 2020, "Black");
            cars[3] = new Car("BMW", "I3", 2000, "Blue");
            cars[4] = new Car("Lamborine", "Diamond", 2019, "White");
            cars[5] = new Car("Suzuki", "abc", 2018, "Green");
            cars[6] = new Car("Mazda", "Mazda-6", 2020, "Orange");
            cars[7] = new Car("Vokagen", "Minicuper", 2021, "Pink");
            cars[8] = new Car("Diamond", "Super Diamond", 2021, "Gold");
            cars[9] = new Car("Apple", "IP", 2021, "White");
        }

        public void PrintCarInfo()
        {
            foreach (Car car in cars)
            {
                Console.WriteLine($"Make: {car.Make}");
                Console.WriteLine($"Model: {car.Model}");
                Console.WriteLine($"Year: {car.Year}");
                Console.WriteLine($"Color: {car.Color}");
                Console.WriteLine("----------------------");
            }
        }
    }
}