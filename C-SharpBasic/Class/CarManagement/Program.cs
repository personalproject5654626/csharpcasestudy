﻿using CarManagement;
void GetMenu()
{
    Console.WriteLine("****** Car Management *******");
    Console.WriteLine("1. Show all Car ");
    Console.WriteLine("2. Exit ");
}
int choice;
do
{
    Car car = new Car();
    GetMenu();
    Console.WriteLine("Enter your choice: ");
    choice = int.Parse(Console.ReadLine()!);
    switch (choice)
    {
        case 1:
            car.ExistCars();
            car.PrintCarInfo();
            break;
        case 2:
            Console.WriteLine("Exits");
            break;
        default:
            break;
    }
} while (choice != 2);

