using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentManagement_Page53
{
    public class Student
    {
        public string? Name { get; set; }
        public int Age { get; set; }
        public int Grade { get; set; }
        public Student(string name, int age, int grade)
        {
            Name = name;
            Age = age;
            Grade = grade;
        }
        public int SetGrade(int grade)
        {
            return Grade = grade;
        }
        public void GetInfo()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Age: {Age}");
            Console.WriteLine($"Grade: {Grade}");
        }

    }
}