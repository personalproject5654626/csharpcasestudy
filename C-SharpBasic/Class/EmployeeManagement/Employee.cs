using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement
{
    public class Employee
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Age { get; set; }
        public double Salary { get; set; }

        public Employee()
        {
            Id = 0;
            Name = "Unknown";
            Age = 20;
            Salary = 5000.0;
        }
        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public Employee(int id, string name, int age, double salary)
        {
            Id = id;
            Name = name;
            Age = age;
            Salary = salary;
        }

        public void DisplayEmplyeeInfo()
        {
            Console.WriteLine($"Employee Id: {Id}");
            Console.WriteLine($"Employee Name: {Name}");
            Console.WriteLine($"Employee Age: {Age}");
            Console.WriteLine($"Employee Salary: {Salary}");

        }
    }
}