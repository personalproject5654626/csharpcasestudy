﻿using EmployeeManagement;

Employee employee = new Employee();
employee.DisplayEmplyeeInfo();
Console.WriteLine("------------------------");

Employee employee1 = new Employee(1, "Diamond");
employee1.DisplayEmplyeeInfo();
Console.WriteLine("------------------------");

Employee employee2 = new Employee(2, "Diamond day", 23, 1);
employee2.DisplayEmplyeeInfo();
Console.WriteLine("------------------------");
