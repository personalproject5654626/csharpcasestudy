﻿using CircleManagement;

Circle circle = new Circle(3.5f);
Console.WriteLine("Area: ");
double area = circle.GetArea();
System.Console.WriteLine(area);
System.Console.WriteLine("Circumference: ");

double circumference = circle.GetCircumference();
System.Console.WriteLine(circumference);
Console.WriteLine("-----------------------");


Circle newCricle = new Circle(5.2f);
Console.WriteLine("Area: ");
double areaNewCircle = newCricle.GetArea();
System.Console.WriteLine(area);
System.Console.WriteLine("Circumference: ");
double circumferenceNew = newCricle.GetCircumference();
System.Console.WriteLine(circumferenceNew);
Console.WriteLine("-----------------------");

Circle[] arrayCircle = new Circle[12];

for (int i = 1; i < arrayCircle.Length; i++)
{
    Console.WriteLine($"Circle with Radius {i}");
    arrayCircle[i] = new Circle(i);

    Console.WriteLine($"Area Circle with Radius {i}");
    double areaCircle = arrayCircle[i].GetArea();
    Console.WriteLine(areaCircle);

    Console.WriteLine($"Circumference Circle with Radius {i}");
    double circumferenceCircle = arrayCircle[i].GetCircumference();
    System.Console.WriteLine(circumferenceCircle);
    Console.WriteLine("-----------------------");

}
