using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CircleManagement
{
    public class Circle
    {
        public float Radius { get; set; }

        public Circle(float radius)
        {
            Radius = radius;
        }

        public double GetArea()
        {
            double area = Math.Round(Math.PI * Math.Pow(Radius, 2));
            return area;
        }

        public double GetCircumference()
        {
            double circumference = Math.Round(Math.PI * 2 * Radius);
            return circumference;
        }
    }
}