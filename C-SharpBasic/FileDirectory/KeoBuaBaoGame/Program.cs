﻿using KeoBuaBaoGame;

KeoBuaBao game = new KeoBuaBao();

int getChoice()
{
    System.Console.WriteLine("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}
void getMenu()
{
    Console.WriteLine("**** Game ****");
    Console.WriteLine("1. Play Keo Bua Bao Game");
    Console.WriteLine("2. Show best score of Player ");
    Console.WriteLine("3. Exit");
    Console.WriteLine("------------------------");
}
int choice;
do
{
    getMenu();
    choice = getChoice();
    switch (choice)
    {
        case 1:
            try
            {
                game.playerChoice();
                game.WriteToFile();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"Error:  {ex.Message}");
            }
            break;
        case 2:
            try
            {
                game.ReadFromFile();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            break;
        case 3:
            Console.WriteLine("Exit");
            break;

    }

} while (choice != 3);