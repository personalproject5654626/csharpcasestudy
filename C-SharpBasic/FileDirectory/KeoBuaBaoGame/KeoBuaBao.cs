using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeoBuaBaoGame
{
    public class KeoBuaBao
    {
        const int KEO = 1;
        const int BUA = 2;
        const int BAO = 3;

        int bestWin = 0;
        int countPlayerOneWin = 0;
        int countPlayerTwoWin = 0;
        string whoWin = "";

        public string playerWinOption(int playerOneChoice, int playerTwoChoice)
        {
            bool playOneWin = playerOneChoice == KEO && playerTwoChoice == BAO
                           || playerOneChoice == BUA && playerTwoChoice == KEO
                           || playerOneChoice == BAO && playerTwoChoice == BUA;
            bool Tie = playerOneChoice == playerTwoChoice;
            if (Tie)
            {
                return whoWin = "Tie";
            }
            else if (playOneWin)
            {
                return whoWin = "PlayerOne";
            }
            else
            {
                return whoWin = "PlayerTwo";
            }
        }
        public int playerChoice()
        {
            Console.WriteLine("Player One choice: ");
            int playerOneOption = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Player Two choice: ");
            int playerTwoOption = int.Parse(Console.ReadLine()!);
            string option = playerWinOption(playerOneOption, playerTwoOption);

            if (option.Contains("Tie"))
            {
                Console.WriteLine("Two player TIE");
            }
            else if (option.Contains("PlayerOne"))
            {
                Console.WriteLine("Player One Win");
                countPlayerOneWin++;
            }
            else
            {
                Console.WriteLine("Player Two Win");
                countPlayerTwoWin++;
            }
            Console.WriteLine("------------------------");

            if (countPlayerOneWin > countPlayerTwoWin)
            {
                bestWin = countPlayerOneWin;
            }
            if (countPlayerOneWin < countPlayerTwoWin)
            {
                bestWin = countPlayerTwoWin;
            }
            return bestWin;
        }

        public bool ExistFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                return true;
            }
            else
            {
                Console.WriteLine("file khong ton tai");
                return false;
            }
        }

        public void WriteToFile()
        {
            string filePath = @"../KeoBuaBaoGame/bestwin.txt";
            if (ExistFile(filePath))
            {
                if (countPlayerOneWin > countPlayerTwoWin)
                {
                    whoWin = "PlayerOne";
                }
                if (countPlayerOneWin < countPlayerTwoWin)
                {
                    whoWin = "PlayerTwo";
                }
                string content = $"Best of {whoWin} score: {bestWin}";
                File.WriteAllText(filePath, content);
            }
            else
            {
                throw new FileNotFoundException("File NOT save!");
            }
        }

        public void ReadFromFile()
        {
            string filePath = @"../KeoBuaBaoGame/bestwin.txt";
            if (ExistFile(filePath))
            {
                string[] lines = File.ReadAllLines(filePath);
                foreach (string line in lines)
                {
                    Console.WriteLine(line);
                }
            }
            else
            {
                throw new FileNotFoundException("File Not Found!");
            }
        }

    }
}
