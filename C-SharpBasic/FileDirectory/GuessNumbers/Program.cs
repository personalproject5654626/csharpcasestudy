﻿using GuessNumbers;

int getChoice()
{
    Console.Write("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}

void getMenu()
{
    Console.WriteLine("******* Guess Number Game *******");
    System.Console.WriteLine("1. Play Game");
    System.Console.WriteLine("2. Show history");
    System.Console.WriteLine("3. Exit");
    System.Console.WriteLine("-------------------------------");
}
int choice;
Games game = new Games();

do
{
    getMenu();
    choice = getChoice();
    switch (choice)
    {
        case 1:
            try
            {
                game.Rule();
                game.WriteToFile();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            break;
        case 2:
            try
            {
                game.ReadFromFile();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            break;
        case 3:
            Console.WriteLine("Exit");
            break;
        default:
            break;
    }
} while (choice != 3);