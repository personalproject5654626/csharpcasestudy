using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuessNumbers
{
    public class Games
    {
        const int BEGIN_NUMBER = 0;
        const int END_NUMBER = 99;

        const string FILE_PATH = @"../GuessNumbers/result.txt";

        int countTimesGuess = 0;
        public int RandomNumber()
        {
            int randomNumber = Random.Shared.Next(BEGIN_NUMBER, END_NUMBER);
            return randomNumber;
        }
        public int GuessNumber()
        {
            Console.Write("Enter number you thing correct:");
            int guessNumber = int.Parse(Console.ReadLine()!);
            if (guessNumber > END_NUMBER && guessNumber < BEGIN_NUMBER)
            {
                throw new Exception($"Guess Number must in {BEGIN_NUMBER} to {END_NUMBER} ");
            }
            return guessNumber;
        }
        public int Rule()
        {
            int randomNumber = RandomNumber();
            int guessNumber = -1;
            do
            {
                guessNumber = GuessNumber();
                if (guessNumber < randomNumber)
                {
                    Console.Write($"Number {guessNumber} is less than Correct Number");
                    Console.WriteLine(" Please guess UP");
                    countTimesGuess++;
                }
                if (guessNumber > randomNumber)
                {
                    Console.Write($"Number {guessNumber} is greater than Correct Number");
                    Console.WriteLine(" Please guess DOWN");
                    countTimesGuess++;
                }
                if (guessNumber == randomNumber)
                {
                    Console.WriteLine($"Number {guessNumber} is CORRECT NUMBER");
                    countTimesGuess++;
                }
            } while (guessNumber != randomNumber);
            return countTimesGuess;
        }

        public bool ExistFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                return true;
            }
            else
            {
                Console.WriteLine("file khong ton tai");
                return false;
            }
        }
        public void WriteToFile()
        {
            if (ExistFile(FILE_PATH))
            {
                string content = $"Times of guess: {countTimesGuess}";
                File.WriteAllText(FILE_PATH, content);
            }
            else
            {
                throw new ArgumentNullException("Can NOT write to file!");
            }
        }
        public void ReadFromFile()
        {
            if (ExistFile(FILE_PATH))
            {
                string[] lines = File.ReadAllLines(FILE_PATH);
                foreach (string line in lines)
                {
                    System.Console.WriteLine(line);
                }
            }
            else
            {
                throw new ArgumentNullException("Can NOT read from file!");
            }
        }
    }
}