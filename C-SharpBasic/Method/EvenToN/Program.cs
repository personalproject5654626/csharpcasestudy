﻿int SumEvenNumbers(int number)
{
    int sum = 0;
    for (int i = 1; i <= number; i++)
    {
        if (i % 2 == 0)
        {
            sum += i;
        }
    }
    return sum;
}

int Input()
{
    System.Console.WriteLine("Enter number : ");
    int number = int.Parse(Console.ReadLine()!);
    return number;
}
void Run()
{
    int result = SumEvenNumbers(Input());
    System.Console.WriteLine($"Sum Even 1 to Number: {result}");
}

Run();