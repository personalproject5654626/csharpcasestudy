﻿
void DispBoard(string[,] board) //creating the board
{
    Console.WriteLine("\t    Tic Tac Toe\n\n");
    Console.WriteLine("         0         1        2    ");
    Console.WriteLine("---------------------------------");
    Console.WriteLine("0       {0}   |   {1}   |   {2}   ", board[0, 0], board[0, 1], board[0, 2]);
    Console.WriteLine("---------------------------------");
    Console.WriteLine("1       {0}   |   {1}   |   {2}   ", board[1, 0], board[1, 1], board[1, 2]);
    Console.WriteLine("---------------------------------");
    Console.WriteLine("2       {0}   |   {1}   |   {2}    ", board[2, 0], board[2, 1], board[2, 2]);
    Console.WriteLine("---------------------------------");
}

string[,] PlaceMarker(string[,] board)
{
    int playerTurn = 1;
    int x_coord;
    int y_coord;

    for (int i = 0; i < 9; i++)
    {
        Console.WriteLine("\nPlayer {0}, please enter a row number: ", playerTurn);
        x_coord = int.Parse(Console.ReadLine()!);
        Console.WriteLine("Now, enter the column number: ");
        y_coord = int.Parse(Console.ReadLine()!);

        if (playerTurn == 1)
        {
            board[x_coord, y_coord] = "X";
            playerTurn = 2;
        }
        else if (playerTurn == 2)
        {
            board[x_coord, y_coord] = "O";
            playerTurn = 1;
        }
        DispBoard(board);
        bool winner = CheckWin(board);
        if (winner)
        {
            break;
        }
    }
    return board;
}

bool CheckWin(string[,] board)
{
    if (board[0, 0] == board[0, 1] && board[0, 1] == board[0, 2] && board[0, 0] != "*")
    {
        Console.WriteLine(board[0, 0] + " wins!");
        return true;
    }
    else if (board[1, 0] == board[1, 1] && board[1, 1] == board[1, 2] && board[1, 0] != "*")
    {
        Console.WriteLine(board[1, 0] + " wins!");
        return true;
    }
    else if (board[2, 0] == board[2, 1] && board[2, 1] == board[2, 2] && board[2, 0] != "*")
    {
        Console.WriteLine(board[2, 0] + " wins!");
        return true;
    }
    else if (board[0, 0] == board[1, 0] && board[1, 0] == board[2, 0] && board[0, 0] != "*")
    {
        Console.WriteLine(board[0, 0] + " wins!");
        return true;
    }
    else if (board[0, 1] == board[1, 1] && board[1, 1] == board[2, 1] && board[0, 1] != "*")
    {
        Console.WriteLine(board[0, 1] + " wins!");
        return true;
    }
    else if (board[0, 2] == board[1, 2] && board[1, 2] == board[2, 2] && board[0, 2] != "*")
    {
        Console.WriteLine(board[0, 2] + " wins!");
        return true;
    }
    else if (board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2] && board[0, 0] != "*")
    {
        Console.WriteLine(board[0, 0] + " wins!");
        return true;
    }
    else if (board[0, 2] == board[1, 1] && board[0, 1] == board[2, 0] && board[0, 2] != "*")
    {
        Console.WriteLine(board[0, 2] + " wins!");
        return true;
    }
    return false;
}//end WinCheck
void Main()
{

    //create 2D string
    string[,] board = new string[3, 3] { { "*", "*", "*" }, { "*", "*", "*" }, { "*", "*", "*" } };

    DispBoard(board);
    PlaceMarker(board);



}//end main
Main();