﻿const int FIRST_ORE = 10;
const int FIRST_ORE_PRICE = 10;
const int SECOND_ORE = 5;
const int SECOND_ORE_PRICE = 5;
const int THIRD_ORE = 3;
const int THIRD_ORE_PRICE = 2;


int goldOfOre(int quantity)
{
    int totalGold = 0;
    int firstGold = 0;
    int secondGold = 0;
    int thirdGold = 0;
    int existGold = 0;
    for (int i = 0; i <= quantity; i++)
    {
        if (i <= FIRST_ORE)
        {
            firstGold = i * FIRST_ORE_PRICE;
        }
        else if (i > FIRST_ORE && i <= FIRST_ORE + SECOND_ORE)
        {
            secondGold = (i - FIRST_ORE) * SECOND_ORE_PRICE;
        }
        else if (i > SECOND_ORE && i <= (FIRST_ORE + SECOND_ORE + THIRD_ORE))
        {
            thirdGold = (i - (FIRST_ORE + SECOND_ORE)) * THIRD_ORE_PRICE;
        }
        else
        {
            existGold = quantity - (FIRST_ORE + SECOND_ORE + THIRD_ORE);
        }
    }
    totalGold = firstGold + secondGold + thirdGold + existGold;
    return totalGold;
}

int getChoice()
{
    System.Console.WriteLine("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}
int getQuantity()
{
    System.Console.WriteLine("Enter your quantity: ");
    int quantity = int.Parse(Console.ReadLine()!);
    return quantity;
}
void getMenu()
{
    System.Console.WriteLine("**** Ore Game ****");
    System.Console.WriteLine("1. Play Game");
    System.Console.WriteLine("2. Exit");
    System.Console.WriteLine("------------------------");
}

void runGame()
{
    int choice;
    do
    {
        getMenu();
        choice = getChoice();
        switch (choice)
        {
            case 1:
                int result = goldOfOre(getQuantity());
                System.Console.WriteLine($"Total gold of Ore you have {result}");
                break;
            case 2:
                System.Console.WriteLine("Exit Game");
                break;
            default:
                break;
        }
    } while (choice != 2);
}

//Main
runGame();