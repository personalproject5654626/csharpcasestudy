﻿
int FindLargestNumber(int[] array)
{
    int maxNumber = array[0];
    for (int i = 1; i < array.Length; i++)
    {
        if (array[i] > maxNumber)
        {
            maxNumber = array[i];
        }
    }
    return maxNumber;
}

int[] Input()
{
    int[] array = { 1, 4, 5, 2, 6, 3, 6, 32, 2, 5 };
    return array;
}

void Run()
{
    int result = FindLargestNumber(Input());
    System
    .Console.WriteLine($"Largest Number in Array: {result}");
}
Run();
