﻿int[] exitsArray = new int[] { 111111, 222222, 333333 };

int AddNewLottery(int[] array)
{
    Array.Resize(ref exitsArray, exitsArray.Length + 1);
    System.Console.WriteLine("Enter new Lottery: ");
    int newLottery = int.Parse(Console.ReadLine()!);
    exitsArray[exitsArray.Length - 1] = newLottery;
    int result = exitsArray[exitsArray.Length - 1];
    return result;
}
void PrintExistLottery()
{
    foreach (int num in exitsArray)
    {
        System.Console.WriteLine(num);
    }
}

void Menu()
{
    System.Console.WriteLine("***** Lottery Management *****");
    System.Console.WriteLine("1. Show exist Lottery");
    System.Console.WriteLine("2. Add new Lottery");
    System.Console.WriteLine("3. Exit");
}
int GetChoice()
{
    System.Console.WriteLine("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine()!);
    return choice;
}
void Run()
{
    int choice;
    do
    {
        Menu();
        choice = GetChoice();
        switch (choice)
        {
            case 1:
                System.Console.WriteLine("--------------------");
                System.Console.WriteLine("Exist Lottery:");
                PrintExistLottery();
                System.Console.WriteLine("--------------------");
                break;
            case 2:
                System.Console.WriteLine("--------------------");
                AddNewLottery(exitsArray);
                PrintExistLottery();
                System.Console.WriteLine("--------------------");
                break;
            case 3:
                System.Console.WriteLine("Exit");
                break;
            default:
                break;
        }
    } while (choice != 3);
}

Run();


