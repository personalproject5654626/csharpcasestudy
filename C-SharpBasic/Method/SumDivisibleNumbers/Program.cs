﻿int[] InputArray()
{
    int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    return array;
}

int[] InputNumberDivides()
{
    int[] numbers = { 2, 3 };
    return numbers;
}

int SumDivisibleNumbers(int[] array, int[] numberDivide)
{
    int sum = 0;
    foreach (int number in array)
    {

        if (number % numberDivide[0] == 0)
        {
            sum += number;
        }
        else if (number % numberDivide[1] == 0)
        {
            sum += number;
        }
    }
    return sum;
}

void Run()
{
    int result = SumDivisibleNumbers(InputArray(), InputNumberDivides());
    System.Console.WriteLine($"Sum Divisible Number: {result}");
}

Run();