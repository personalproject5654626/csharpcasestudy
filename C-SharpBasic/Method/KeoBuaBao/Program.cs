﻿const int KEO = 1;
const int BUA = 2;
const int BAO = 3;

int playerWinOption(int playerOneChoice, int playerTwoChoice)
{
    bool playOneWin = (playerOneChoice == KEO) && (playerTwoChoice == BAO)
                   || (playerOneChoice == BUA) && (playerTwoChoice == KEO)
                   || (playerOneChoice == BAO) && (playerTwoChoice == BUA);
    bool Tie = playerOneChoice == playerTwoChoice;
    if (Tie)
    {
        return 0;
    }
    else if (playOneWin)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

int getChoiceMenu()
{
    System.Console.WriteLine("Enter your choice: ");
    int chocie = int.Parse(Console.ReadLine()!);
    return chocie;
}

void getMenu()
{
    System.Console.WriteLine("**** Keo Bua Bao Game ****");
    System.Console.WriteLine("1. Play Game");
    System.Console.WriteLine("2. Exit");
    System.Console.WriteLine("------------------------");
}
void playerChoice()
{
    System.Console.WriteLine("Player One choice: ");
    int playerOneOption = int.Parse(Console.ReadLine()!);
    System.Console.WriteLine("Player Two choice: ");
    int playerTwoOption = int.Parse(Console.ReadLine()!);
    int option = playerWinOption(playerOneOption, playerTwoOption);
    if (option == 0)
    {
        System.Console.WriteLine("Two player TIE");
    }
    else if (option == 1)
    {
        System.Console.WriteLine("Player One Win");
    }
    else
    {
        System.Console.WriteLine("Player Two Win");
    }
    System.Console.WriteLine("------------------------");
}


void runGame()
{
    int chocie;
    do
    {
        getMenu();
        chocie = getChoiceMenu();
        switch (chocie)
        {
            case 1:
                playerChoice();
                break;
            case 2:
                System.Console.WriteLine("Exit Game");
                break;
            default:
                break;
        }
    } while (chocie != 2);
}
//Main
runGame();




