﻿
int FindSecondLargestNumber(int[] array)
{
    int maxNumber = int.MinValue;
    int secondMaxNumber = int.MinValue;
    foreach (int number in array)
    {
        if (number > maxNumber)
        {
            secondMaxNumber = maxNumber;
            maxNumber = number;
        }
        else if (number > secondMaxNumber && number != maxNumber)
        {
            secondMaxNumber = number;
        }
    }
    return secondMaxNumber;
}

int[] Input()
{
    int[] array = { 1, 4, 5, 2, 23, 3, 6, 32, 2, 5 };
    return array;
}

void Run()
{
    int result = FindSecondLargestNumber(Input());
    System
    .Console.WriteLine($"Largest Number in Array: {result}");
}
Run();
