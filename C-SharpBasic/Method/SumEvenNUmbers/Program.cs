﻿int[] Input()
{
    int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    return array;
}

int SumEvenNumbers(int[] array)
{
    int sum = 0;
    for (int i = 0; i < array.Length; i++)
    {
        if (i % 2 == 0)
        {
            sum += i;
        }
    }
    return sum;
}

void Run()
{
    int result = SumEvenNumbers(Input());
    System.Console.WriteLine($"Sum of even numbers in array: {result}");
}
Run();