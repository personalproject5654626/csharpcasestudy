using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Games
{
    public class Game
    {
        const int KEO = 1;
        const int BUA = 2;
        const int BAO = 3;

        const int FIRST_ORE = 10;
        const int FIRST_ORE_PRICE = 10;
        const int SECOND_ORE = 5;
        const int SECOND_ORE_PRICE = 5;
        const int THIRD_ORE = 3;
        const int THIRD_ORE_PRICE = 2;


        public int getQuantity()
        {
            System.Console.WriteLine("Enter your quantity: ");
            int quantity = int.Parse(Console.ReadLine()!);
            return quantity;
        }
        public int OreGames(int quantity)
        {
            if (quantity < 0)
            {
                throw new ArgumentException("Quantity of Ore is less than O! Please enter again!");
            }
            int totalGold = 0;
            int firstGold = 0;
            int secondGold = 0;
            int thirdGold = 0;
            int existGold = 0;
            for (int i = 0; i <= quantity; i++)
            {
                if (i <= FIRST_ORE)
                {
                    firstGold = i * FIRST_ORE_PRICE;
                }
                else if (i > FIRST_ORE && i <= FIRST_ORE + SECOND_ORE)
                {
                    secondGold = (i - FIRST_ORE) * SECOND_ORE_PRICE;
                }
                else if (i > SECOND_ORE && i <= (FIRST_ORE + SECOND_ORE + THIRD_ORE))
                {
                    thirdGold = (i - (FIRST_ORE + SECOND_ORE)) * THIRD_ORE_PRICE;
                }
                else
                {
                    existGold = quantity - (FIRST_ORE + SECOND_ORE + THIRD_ORE);
                }
                totalGold = firstGold + secondGold + thirdGold + existGold;
            }
            return totalGold;
        }

        public int playerWinOption(int playerOneChoice, int playerTwoChoice)
        {
            bool playOneWin = (playerOneChoice == KEO) && (playerTwoChoice == BAO)
                           || (playerOneChoice == BUA) && (playerTwoChoice == KEO)
                           || (playerOneChoice == BAO) && (playerTwoChoice == BUA);
            bool Tie = playerOneChoice == playerTwoChoice;
            if (Tie)
            {
                return 0;
            }
            else if (playOneWin)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        public void playerChoice()
        {
            System.Console.WriteLine("Player One choice: ");
            int playerOneOption = int.Parse(Console.ReadLine()!);
            System.Console.WriteLine("Player Two choice: ");
            int playerTwoOption = int.Parse(Console.ReadLine()!);
            int option = playerWinOption(playerOneOption, playerTwoOption);

            bool errorChocie = playerOneOption != KEO && playerOneOption != BUA && playerOneOption != BAO
                            || playerTwoOption != KEO && playerTwoOption != BUA && playerTwoOption != BAO;


            if (errorChocie)
            {
                throw new ArgumentException("Your choice OutOfRange, Please chocie again!");

            }
            if (option == 0)
            {
                System.Console.WriteLine("Two player TIE");
            }
            else if (option == 1)
            {
                System.Console.WriteLine("Player One Win");
            }
            else
            {
                System.Console.WriteLine("Player Two Win");
            }
            System.Console.WriteLine("------------------------");
        }

    }


}

