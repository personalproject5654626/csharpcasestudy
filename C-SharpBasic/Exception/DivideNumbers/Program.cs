﻿int DivideNumbers(int firstNumber, int secondNumber)
{
    int result;
    if (secondNumber == 0)
    {
        throw new DivideByZeroException("DivideByZeroException");
    }
    result = firstNumber / secondNumber;
    return result;
}


try
{
    Console.WriteLine("Enter first Number: ");
    int firstNumber = int.Parse(Console.ReadLine()!);
    Console.WriteLine("Enter second Number: ");
    int secondNumber = int.Parse(Console.ReadLine()!);

    int resultDivide = DivideNumbers(firstNumber, secondNumber);
    System.Console.WriteLine($"Result Divide: {resultDivide}");
}
catch (DivideByZeroException dex)
{
    Console.WriteLine($"Error: {dex.Message}");
}
catch (System.Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
}